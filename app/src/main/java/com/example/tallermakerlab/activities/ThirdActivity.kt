package com.example.tallermakerlab.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.tallermakerlab.R
import com.example.tallermakerlab.ViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_second.*


class ThirdActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        viewPager.adapter = viewPagerAdapter
    }
}